---
title: "Medidas de centralizacion"
date: 2019-08-19T12:54:44-05:00
draft: false
---
# Medidas de Centralización

- Media Aritmetica
- Mediana
- Moda
- Percentiles


## Media Aritmetica

La Media aritmetica o promedio es el equilibrio de las unidades entre todos sus participantes, es decir logramos que el participante con mayor unidades este al mismo nivel que el de menor unidades



|Niños|Manzanas|
|:----|:-------|
|1    |3       |
|2    |2       |
|3    |4       |
|4    |2       |
|5    |4       |

